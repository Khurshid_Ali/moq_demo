using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.Json;
using System.Threading.Tasks;
using Castle.Core.Internal;
using Moq;
using SalesforceAdapter;
using SalesforceAdapter.Client;
using Xunit;

namespace SalesforceAdapterTests
{
    public class EntityServiceTest : IDisposable
    {
        /*
         * Constructor of the Test Class
         *
         * Use the constructor to setup for all tests in the class.
         */
        public EntityServiceTest()
        {
            
        }


        /*
         * IDisposable interface.
         * Dispose of any resources that were created 
         */
        public void Dispose()
        {
            
        }

        public JsonDocument BuildResponse()
        {
           
            var entity = new Dictionary<string, object>();
            entity.Add("Id", "abc123");
            entity.Add("Type", "Contact");
            entity.Add("Name", "Khurshid Ali");
            entity.Add("Rating", 5);
            entity.Add("LastUpdated", DateTime.Now.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssK"));

            return JsonDocument.Parse(JsonSerializer.Serialize(entity));    
        }
        
        
        [Fact]
        public async Task GetEntity_Authentication_Failed_Throw_Exception()
        {
            //1. Mock Dependencies
            var mockCilent = new Mock<ISalesforceClient>();
            mockCilent
                .Setup(x => x.AuthenticateAsync(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(false);
            
            //2. The Actual Test 
            var service = new EntityService(mockCilent.Object);
            
            //to capture the exception thrown we will use an act variable
            Task act() => service.GetEntityAsync("Contact", "key");
            
            //3. Assertions
            var exception = await Assert.ThrowsAsync<Exception>(act);
            
            Assert.Equal("Authentication Failed", exception.Message);

        }

        

        /*
         * [Fact] decorator makes this method a test method.
         */
        [Fact]
        public async Task GetEntity_Authentication_Success_retrieve_entity()
        {
           //Each Test has three parts 
           
           //1. Mocking Dependencies 
           
           var mockCilent = new Mock<ISalesforceClient>();

           //mock the authenticate function to return true.
           mockCilent
               .Setup(x => x.AuthenticateAsync(It.IsAny<string>(), It.IsAny<string>()))
               .ReturnsAsync(true);

           mockCilent
               .Setup(x => x.GetEntityAsync(It.IsAny<string>(), It.IsAny<string>()))
               .ReturnsAsync(BuildResponse());
               
           
           
           //2. The actual Test

           var service = new EntityService(mockCilent.Object);
           var actual = await service.GetEntityAsync("Contact", "abc123");

           //3. Assertions 
           Assert.NotNull(actual);
           
           //Assert Id 
           Assert.Equal("abc123", actual.Id);
           
           //assert Name
           Assert.False(actual.Name.IsNullOrEmpty());
           Assert.Equal("Khurshid Ali", actual.Name);
           
           Assert.InRange(actual.Rating, 3, 7);
           
           //last updated in last two days.
           Assert.InRange(actual.LastUpdated, DateTime.Now.AddDays(-1), DateTime.Now.AddDays(+1) );
           
           
        }

        
        
        
    }
}