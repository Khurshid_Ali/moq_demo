using System.Text.Json;
using System.Threading.Tasks;

namespace SalesforceAdapter.Client
{
    public interface ISalesforceClient
    {
        Task<bool> AuthenticateAsync(string userName, string password);
        Task<JsonDocument> GetEntityAsync(string entityName, string entityKey);
        

    }
}