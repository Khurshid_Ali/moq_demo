using System;
using System.Text.Json;
using System.Threading.Tasks;
using SalesforceAdapter.Client;
using SalesforceAdapter.Models;

namespace SalesforceAdapter
{
    public class EntityService
    {

        private ISalesforceClient _client;
        
        /*
         * Decouple, use Injection
         * don't use new keyword.
         * 
         */
        public EntityService(ISalesforceClient client)
        {
            _client = client;
        }


        /*
         *
         *
         * 
         */
        public async Task<Entity> GetEntityAsync(string entityType, string entityKey)
        {
            
            //authenticate 
            
            //if authentication failure raise exception
            var authResult = await _client.AuthenticateAsync("", "");

            if (!authResult)
            {
                throw new Exception("Authentication Failed");
            }
            
            // if authentication is successfull 
            var jsonDoc = await _client.GetEntityAsync(entityType, entityKey);



            var entity = JsonSerializer.Deserialize<Entity>(jsonDoc.RootElement.GetRawText());
            
            
            return entity;
        }


        
    }
}