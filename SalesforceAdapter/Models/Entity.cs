using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Text.Json.Serialization;

namespace SalesforceAdapter.Models
{
    public class Entity
    {
        [JsonPropertyName("Name")]
        public string Name { get; set; }
        
        [JsonPropertyName("Id")]
        public string Id { get; set; }
        
        [JsonPropertyName("Type")]
        public string Type { get; set; }
        
        [JsonPropertyName("LastUpdated")]
        public DateTime LastUpdated { get; set; }
        
        [JsonPropertyName("Rating")]
        public int Rating { get; set; }

    }

    
    
}